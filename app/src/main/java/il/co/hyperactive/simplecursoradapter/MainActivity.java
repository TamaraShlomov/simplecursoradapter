package il.co.hyperactive.simplecursoradapter;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    EditText nameEditText, familyEditText;
    SQLiteDatabase database;
    SimpleCursorAdapter adapter;
    private static String DATABASE_NAME = "NamesDataBase";
    private static String TABLE_NAME = "tbl_names";
    private static String TABLE_ROW_NAME = "name";
    private static String TABLE_ROW_FAMILY = "family";
    Cursor myCursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database =  openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        String CreateTableCommand = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TABLE_ROW_NAME + " TEXT, " +TABLE_ROW_FAMILY+ " TEXT);";
        database.execSQL(CreateTableCommand);
        initViews();
        findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameEditText.getText().toString();
                String family = familyEditText.getText().toString();
                ContentValues values = new ContentValues();
                values.put(TABLE_ROW_NAME, name);
                values.put(TABLE_ROW_FAMILY, family);
                database.insert(TABLE_NAME, null, values);
            }
        });
        findViewById(R.id.buttonShow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myCursor != null)
                {
                    myCursor.close();
                }
                myCursor = database.query(TABLE_NAME, null, null, null, null, null, null);
                String[] from = new String[]{TABLE_ROW_NAME,TABLE_ROW_FAMILY};
                int[] to = new int[]{R.id.textViewName,R.id.textViewFamily};
                adapter = new SimpleCursorAdapter(MainActivity.this,R.layout.linecontent,myCursor,from,to,
                        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
                ((ListView)findViewById(R.id.listView)).setAdapter(adapter);

            }
        });
        findViewById(R.id.buttonSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] cols = new String[]{"_id",TABLE_ROW_NAME,TABLE_ROW_FAMILY};
                String where = TABLE_ROW_NAME +" LIKE '" + nameEditText.getText().toString() +"%'";
                if(myCursor != null)
                {
                    myCursor.close();
                }
                myCursor = database.query(TABLE_NAME, cols, where, null, null, null, null);
                String[] from = new String[]{TABLE_ROW_NAME,TABLE_ROW_FAMILY};
                int[] to = new int[]{R.id.textViewName,R.id.textViewFamily};
                adapter = new SimpleCursorAdapter(MainActivity.this,R.layout.linecontent,myCursor,from,to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
                ((ListView)findViewById(R.id.listView)).setAdapter(adapter);

            }
        });

    }

    private void initViews()
    {
        nameEditText = (EditText)findViewById(R.id.editTextName);
        familyEditText = (EditText)findViewById(R.id.editTextFamily);
    }

}
